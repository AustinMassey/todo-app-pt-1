import React, { useState } from "react";
import todosList from "./todos.json";
import TodoList from "./components/todoList/TodoList";
import TodoItem from "./components/todoItem/TodoItem";


function App() {
  const [todos, setTodos] = useState(todosList)
  const handleKeyPress = event => {
    if (event.key === "Enter") {
      event.preventDefault();
      newTodo();
    }
  }
  const handleDeleteCompleted = (id) => {
    const newTodos = todos.filter(item =>(item.completed!== true)
    )
  setTodos(newTodos)}
  const handleDelete = (id) => {
    const newTodos = todos.filter(item =>(item.id!== id)
    )
  setTodos(newTodos)}
  const handleToggle = (id) => {
    const newTodos = todos.map(item => {
      if (item.id === id) {
        item.completed = !item.completed
      }
      return item
    }
    )
    setTodos(newTodos) }
    const newTodo = () => {
      let item = document.getElementById("TodosId").value
      if (item.length < 2) {
        return
      }

      let arrayLength = todos.length
      let newId = todos[arrayLength - 1].id + 1
      let newItems = {
        userId: 1,
        id: newId,
        title: item,
        completed: false
      }
      let newItem = [...todos]
      newItem.push(newItems)
      document.getElementById("TodosId").value = ''
      setTodos(newItem)
    }
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            onKeyPress={handleKeyPress}
            id="TodosId"
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus />
        </header>
        <TodoList todos={todos} handleToggle={handleToggle}
        handleDelete = {handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick = {handleDeleteCompleted}
          className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }





  export default App;
