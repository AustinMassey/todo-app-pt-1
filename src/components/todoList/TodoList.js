import React from "react";
import TodoItem from "../todoItem/TodoItem";


function TodoList(props) {
  
    return (
      <section className="main">
        <ul className="todo-list">
          {props.todos.map((todo) => (
            <TodoItem title={todo.title} 
            handleToggle={props.handleToggle} 
            completed={todo.completed} 
            id={todo.id} key={todo.id} 
            handleDelete={props.handleDelete}/>
          ))}
        </ul>
      </section>
    );
  }

export default TodoList;